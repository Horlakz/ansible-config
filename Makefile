repo_name ?= $(shell bash -c 'read -p "Repo Name(e.g username/repo_name): " repo_name; echo $$repo_name')
domain ?= $(shell bash -c 'read -p "Domain Name(add &apos; if more than one): " domain; echo $$domain')
port ?= $(shell bash -c 'read -p "Docker Port: " port; echo $$port')
dir ?= $(shell bash -c 'read -p "Folder(optional): " dir; echo $$dir')
github_token ?= $(shell bash -c 'read -p "Config Token: " github_token; echo $$github_token')
actions_folder ?= $(shell bash -c 'read -p "Actions Folder Dir: " actions_folder; echo $$actions_folder')

USERNAME ?= $(shell bash -c 'read -p "Username: " username; echo $$username')
PASSWORD ?= $(shell bash -c 'read -s -p "Password: " pwd; echo $$pwd')

talk:
	@clear
	@echo Username › $(USERNAME)
	@echo Password › $(PASSWORD)

ping:
	ansible all -m ping

# run all playbooks
all: add_public_keys update_server install_docker set_cron_jobs

# run playbooks individually
update_server:
	@ansible-playbook playbooks/update-server.yaml

add_public_keys:
	@ansible-playbook playbooks/add_public_keys.yaml -K

install_docker:
	@ansible-playbook playbooks/install-docker.yaml

nginx_and_ssl_config:
	@ansible-playbook playbooks/nginx_and_ssl_config.yaml

set_cron_jobs:
	@ansible-playbook playbooks/set_cron_jobs.yaml

configure_mongo_db:
	@ansible-playbook playbooks/configure_mongo_db.yaml

install_github_actions:
	@ansible-playbook playbooks/install_github_actions.yaml

remove_github_actions:
	@ansible-playbook playbooks/remove_github_actions.yaml --extra-vars="github_token=$(github_token) actions_folder=$(actions_folder)"

generate_files:
	@node ./generate-ansible-files/index --repo $(repo_name) --domain $(domain) --port $(port) --dir $(dir)

make auto_deploy: generate_files install_github_actions nginx_and_ssl_config
